

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class showFrameModel extends AbstractTableModel {
    
    String[] columnName = {"Name","Surname","Username","Tel","Weight","Height"};
    ArrayList<User> userList = User.userList;
    public showFrameModel (){
        
    }
    
    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }

    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User user = userList.get(rowIndex);
        switch(columnIndex){
            case 0: return user.getName();
            case 1: return user.getSurname();
            case 2: return user.getUsername();
            case 3: return user.getTel();
            case 4: return user.getWeight();
            case 5: return user.getHeight();
        }
        return "";
    }
    
}
