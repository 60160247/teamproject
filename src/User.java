
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
public class User {

    static ArrayList<User> userList = new ArrayList<>();
    private String name;
    private String surname;
    private String username;
    private String password;
    private String tel;
    private String weight;
    private String height;
    static User[] username_checked = new User[1];

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTel() {
        return tel;
    }

    public String getWeight() {
        return weight;
    }

    public String getHeight() {
        return height;
    }

    public User(String name, String surname, String username, String password, String tel, String weight, String height) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    /*public static void load() {
        userList.add(new User("admin", "admin", "admin", "admin", "0999999999", "60.0", "175.0"));
    }*/

 /*public static ArrayList<User> getUserList(){
        return userList;
    }*/
    public static String checkError(User new_userList) {
        for (User a : userList) {
            if (a.getName().equals(new_userList.getName()) && a.getSurname().equals(new_userList.getSurname())) {
                return "Error name and surname is exist";
            } else if (a.getUsername().equals(new_userList.getUsername())) {
                return "Error username is exist";
            } else {
                try {
                    Double.parseDouble(new_userList.getWeight());
                    Double.parseDouble(new_userList.getHeight());
                } catch (Exception e) {
                    return "Error weight and height must be number";
                }
            }
        }
        userList.add(new_userList);
        return "";
    }

    public static User checkUsername(String username) {
        for (User a : userList) {
            if (a.getUsername().equals(username)) {
                username_checked[0].name = a.getName();
                username_checked[0].surname = a.getSurname();
                username_checked[0].username = a.getUsername();
                username_checked[0].password = a.getPassword();
                username_checked[0].tel = a.getTel();
                username_checked[0].weight = a.getWeight();
                username_checked[0].height = a.getHeight();
                return username_checked[0];
            }
        }
        return null;
    }

    public static void add(String name, String surname, String username, String password, String tel, String weight, String height) {
        userList.add(new User(name, surname, username, password, tel, weight, height));
    }
}
